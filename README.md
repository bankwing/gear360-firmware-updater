# Gear360 Firmware Updater

1. First download the Firmware file from here : https://forum.xda-developers.com/attachments/r210glu0arb2_180201_1739_rev00_user-bin.5193015/
2. Copy "R210GLU0ARB2_180201_1739_REV00_user.bin" file to the SD card root
3. Copy "updater.sh" into SD card root
4. Power Gear360 and wait for firmware update
